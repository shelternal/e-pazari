// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
     apiKey: "AIzaSyAGb88iCL0kPIy2bvZ5eWfbKgHRmI0KqLg",
     authDomain: "e-pazar.firebaseapp.com",
     projectId: "e-pazar",
     storageBucket: "e-pazar.appspot.com",
     messagingSenderId: "312915645616",
     appId: "1:312915645616:web:43133e1daaa3a3b4bb9775",
     measurementId: "G-CBEK3MKV5M"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
