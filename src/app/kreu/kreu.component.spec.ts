import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KreuComponent } from './kreu.component';

describe('KreuComponent', () => {
  let component: KreuComponent;
  let fixture: ComponentFixture<KreuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KreuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KreuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
