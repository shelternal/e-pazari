import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegjistrimiCComponent } from './regjistrimi-c.component';

describe('RegjistrimiCComponent', () => {
  let component: RegjistrimiCComponent;
  let fixture: ComponentFixture<RegjistrimiCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegjistrimiCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegjistrimiCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
