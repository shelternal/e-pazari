import { Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators,ReactiveFormsModule} from "@angular/forms";
import {forbiddenNameValidator} from './shared/user-name.validator';
import {PasswordValidator} from './shared/password.validator';
import {UserServiceService} from "../user-service.service";
import {AngularFirestore} from "@angular/fire/firestore";


@Component({
  selector: 'app-regjistrimi-c',
  templateUrl: './regjistrimi-c.component.html',
  styleUrls: ['./regjistrimi-c.component.css']
})
export class RegjistrimiCComponent implements OnInit {


  // @ts-ignore
  registrationForm: FormGroup ;

  get userName() {
    return this.registrationForm.get('userName');

  }

  get email() {
    return this.registrationForm.get('email');
  }

  get rolet() {
    return this.registrationForm.get('rolet');
  }
  get zonat() {
    return this.registrationForm.get('zonat');
  }


  isValidated = false;

  Rolet : any = ['Shites', 'Bleres']
  Zonat: any = ['Tirane e Re', 'Shkolla e Bashkuar', 'Pazar i Ri', 'Blloku']

  constructor(private fb: FormBuilder,
   public service : UserServiceService,
   private firestore: AngularFirestore) {
  }

  ngOnInit() {

    this.registrationForm = this.fb.group({

      userName: ['', [Validators.required, Validators.minLength(3), forbiddenNameValidator(/password/)]],

      email: ['', [Validators.required]],

      rolet: ['', [Validators.required]],

      password: ['',[Validators.required]],
      confirmPassword: ['',[Validators.required]],


      zonat: ['', [Validators.required]],

    }, {validator: PasswordValidator} );

  }



  // @ts-ignore
  changeRoles(e)  {
    this.rolet?.setValue(e.target.value, {
      onlySelf: true
    })
  }
  // @ts-ignore
  changeZone(e)  {
    this.zonat?.setValue(e.target.value, {
      onlySelf: true
    })
  }

  onSubmit(form:FormGroup) {
      let data = form.value;
      console.log(form);
      this.firestore.collection('Users').add(data)


  }
  ischecked(){

  }

}
