import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HyrjaCComponent } from './hyrja-c.component';

describe('HyrjaCComponent', () => {
  let component: HyrjaCComponent;
  let fixture: ComponentFixture<HyrjaCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HyrjaCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HyrjaCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
