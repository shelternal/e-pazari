
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {KreuComponent} from "./kreu/kreu.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {HyrjaCComponent} from "./Regjistrimi/hyrja-c/hyrja-c.component";
import {TePreferuaratComponent} from "./te-preferuarat/te-preferuarat.component";
import {ShportaComponent} from "./shporta/shporta.component";
import {RegjistrimiCComponent} from "./Regjistrimi/regjistrimi-c/regjistrimi-c.component";

const routes: Routes = [
  { path: 'kreu', component: KreuComponent },
  { path: 'tePreferuarat', component:TePreferuaratComponent},
  { path: 'shporta', component: ShportaComponent},
  { path: 'hyr', component: HyrjaCComponent},
  { path: 'regjistrimi', component: RegjistrimiCComponent},

  { path: '', redirectTo: '/regjistrimi', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
