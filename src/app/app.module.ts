import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import {FormsModule} from "@angular/forms";
import {environment} from "../environments/environment";
import {AngularFireModule} from "@angular/fire";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import { AppComponent } from './app.component';
import { HyrjaCComponent } from './Regjistrimi/hyrja-c/hyrja-c.component';
import { KreuComponent } from './kreu/kreu.component';
import {RouterModule} from "@angular/router";
import { NavigimiComponent } from './navigimi/navigimi.component';
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import { TePreferuaratComponent } from './te-preferuarat/te-preferuarat.component';
import { ShportaComponent } from './shporta/shporta.component';
import {RegjistrimiCComponent} from "./Regjistrimi/regjistrimi-c/regjistrimi-c.component";
import { ReactiveFormsModule } from '@angular/forms';

import {UserServiceService} from "./Regjistrimi/user-service.service";

@NgModule({
  declarations: [
    AppComponent,
    HyrjaCComponent,
    KreuComponent,
    NavigimiComponent,
    PageNotFoundComponent,
    TePreferuaratComponent,
    ShportaComponent,
    RegjistrimiCComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [UserServiceService],
  bootstrap: [AppComponent]
})

export class AppModule { }
