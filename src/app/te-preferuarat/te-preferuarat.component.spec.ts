import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TePreferuaratComponent } from './te-preferuarat.component';

describe('TePreferuaratComponent', () => {
  let component: TePreferuaratComponent;
  let fixture: ComponentFixture<TePreferuaratComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TePreferuaratComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TePreferuaratComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
