import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigimiComponent } from './navigimi.component';

describe('NavigimiComponent', () => {
  let component: NavigimiComponent;
  let fixture: ComponentFixture<NavigimiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigimiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigimiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
