import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShportaComponent } from './shporta.component';

describe('ShportaComponent', () => {
  let component: ShportaComponent;
  let fixture: ComponentFixture<ShportaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShportaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShportaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
